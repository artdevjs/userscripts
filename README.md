# Tampermonkey Userscripts

## links

* [Tampermonkey Docs](https://www.tampermonkey.net/documentation.php)

* [Metadata Block Greasemonkey_Manual](https://sourceforge.net/p/greasemonkey/wiki/Metadata_Block/)

* [Greasemonkey Wiki](https://sourceforge.net/p/greasemonkey/wiki/Main_Page/)

  * [Metadata Block](https://wiki.greasespot.net/Metadata_Block)

  * [API reference](https://sourceforge.net/p/greasemonkey/wiki/Greasemonkey_Manual:API)

* [Stylish](https://sourceforge.net/p/greasemonkey/wiki/Security/)

* [Greasy Fork](https://greasyfork.org/en)

* [User Script Compiler](https://arantius.com/misc/greasemonkey/script-compiler) create a Firefox extension (.xpi) from a greasemonkey script

## manuals

* [The Complete User Scripts Guide](https://simply-how.com/enhance-and-fine-tune-any-web-page-the-complete-user-scripts-guide)

# UserStyles

* [Stylus WiKi](https://github.com/openstyles/stylus/wiki)
