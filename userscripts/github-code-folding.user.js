// ==UserScript==
// @name            GitHub Code Folding
// @description     A userscript that adds code folding to GitHub files
// @version         0.1.0
// @author          artdev
// @namespace       namespace of the script
// @homepageURL     authors homepage that is used at the options page to link from the scripts name to the given page
// @supportURL      URL where the user can report issues and get personal support
// @include         https://github.com/*
// @license         License Type; License Homepage
// @run-at          document-idle
// @grant           GM_addStyle
// @icon            https://github.com/favicon.ico
// @require         https://greasemonkey.github.io/gm4-polyfill/gm4-polyfill.js?updated=20180103
// @require         https://greasyfork.org/scripts/28721-mutations/code/mutations.js?version=666427
// ==/UserScript==

/**
 * This userscript has been heavily modified from the "github-code-folding"
 * Chrome extension Copyright 2016 by Noam Lustiger; under an MIT license
 * https://github.com/noam3127/github-code-folding
 */


(() => {
  'use strict';

  GM.addStyle('td.blob-code.blob-code-inner{padding-left:13px}.collapser{position:absolute;left:2px;width:12px;opacity:.5;transition:.15s}.collapser:hover{opacity:1}.sideways{transform:rotate(-90deg);transform-origin:16% 49%;opacity:.8}.hidden-line{display:none}.ellipsis{padding:1px 2px;margin-left:2px;cursor:pointer;background:rgba(255,235,59,.4)}.ellipsis:hover{background:rgba(255,235,59,.7)}');

  const codeLines = [...document.querySelectorAll('table.js-file-line-container tr .blob-code-inner')];
  const codeLinesText = codeLines.map(l => l.textContent);

  const _arrow = '<svg version="1.1" width="7px" fill="#969896" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve"><metadata> Svg Vector Icons : http://www.onlinewebfonts.com/icon </metadata><path d="M579.5,879.8c-43.7,75.7-115.3,75.7-159,0L28.7,201.1c-43.7-75.7-8-137.7,79.5-137.7h783.7c87.5,0,123.2,62,79.5,137.7L579.5,879.8z"></path></svg>';

  class Element {
    constructor(name) {
      this.element = document.createElement(name);
    }
    addClass(className) {
      this.element.classList.add(className)
      return this;
    }
    setId(id) {
      this.element.id = id;
      return this;
    }
    setHTML(str) {
      this.element.innerHTML = str;
      return this;
    }
  }

  const arrowFactory = (id) => {
    return new Element('span')
      .addClass('collapser')
      .setId(id)
      .setHTML(_arrow)
      .element;
  }

  const ellipsisFactory = (id) => {
    return new Element('span')
      .addClass('pl-smi')
      .addClass('ellipsis')
      .setId(id)
      .setHTML('...')
      .element;
  };

  const spaceMap = new Map();
  const pairs = new Map();
  const stack = [];
  const blockStarts = [];
  const countLeadingWhitespace = arr => {
    const getWhitespaceIndex = i => {
      if (arr[i] !== ' ' && arr[i] !== '\t') {
        return i;
      }
      i++;
      return getWhitespaceIndex(i);
    }
    return getWhitespaceIndex(0);
  };

  const last = arr => arr[arr.length - 1];
  const getPreviousSpaces = (map, lineNum) => {
    let prev = map.get(lineNum - 1);
    return prev === -1 ? getPreviousSpaces(map, lineNum - 1) : {lineNum: lineNum - 1, count: prev};
  };

  for (let lineNum = 0; lineNum < codeLinesText.length; lineNum++) {
    let line = codeLinesText[lineNum];
    let count = line.trim().length ? countLeadingWhitespace(line.split('')) : -1;
    spaceMap.set(lineNum, count);

    function tryPair() {
      let top = last(stack);
      if (count !== -1 && count <= spaceMap.get(top)) {
        pairs.set(top, lineNum);
        codeLines[top].setAttribute('block-start', 'true');
        const arrow = arrowFactory(`gcf-${top + 1}`);
        codeLines[top].appendChild(arrow);
        blockStarts.push(codeLines[top]);
        stack.pop();
        return tryPair();
      }
    }
    tryPair();

    let prevSpaces = getPreviousSpaces(spaceMap, lineNum);
    if (count > prevSpaces.count) {
      stack.push(prevSpaces.lineNum);
    }
  }

  const toggleCode = (action, start, end) => {
    if (action === 'hide') {
      const sliced = codeLines.slice(start, end);
      sliced.forEach(elem => {
        let tr = elem.parentNode;
        if (tr.classList) {
          tr.classList.add('hidden-line');
        } else {
          tr.className += ' hidden-line';
        }
        // Check if any children blocks of this parent block have previously
        // been collapsed so we can remove ellipsis and reset arrows
        let previouslyCollapsed = elem.querySelectorAll('.ellipsis');
        previouslyCollapsed.forEach(block => {
          block.previousSibling.classList.remove('sideways');
          block.parentNode.removeChild(block);
        });
      });
      codeLines[start - 1].appendChild(ellipsisFactory(`ellipsis-${start - 1}`));
    } else if (action === 'show') {
      const sliced = codeLines.slice(start, end);
      const topLine = codeLines[start - 1];
      sliced.forEach(elem => {
        let tr = elem.parentNode;
        if (tr.classList) {
          tr.classList.remove('hidden-line');
        } else {
          tr.className.replace(' hidden-line', '');
        }
      });
      topLine.removeChild(topLine.lastChild);
    }
  };

  const arrows = document.querySelectorAll('.collapser');
  function arrowListener(e) {
    e.preventDefault();
    let svg = e.currentTarget;
    let td = e.currentTarget.parentElement;
    let id = td.getAttribute('id');
    let index = parseInt(id.slice(2), 10) - 1;
    if (svg.classList.contains('sideways')) {
      svg.classList.remove('sideways');
      toggleCode('show', index + 1, pairs.get(index));
    } else {
      svg.classList.add('sideways');
      toggleCode('hide', index + 1, pairs.get(index));
    }
  };

  arrows.forEach(c => {
    c.addEventListener('click', arrowListener);
  });

  function ellipsisListener(e) {
    if (!e.target.parentElement) return;
    if (e.target.classList.contains('ellipsis')) {
      let td = e.target.parentElement;
      let svg = td.querySelector('.sideways');
      let id = e.target.parentElement.getAttribute('id');
      let index = parseInt(id.slice(2), 10) - 1;
      svg.classList.remove('sideways');
      toggleCode('show', index + 1, pairs.get(index));
    }
  };

  blockStarts.forEach(line => {
    line.addEventListener('click', ellipsisListener);
  });
})();
