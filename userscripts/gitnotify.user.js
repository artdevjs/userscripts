// ==UserScript==
// @name            GitNotify for Github
// @description     Track Repositories on Git Notify
// @version         0.1.0
// @author          artdev
// @namespace       https://gitlab.com/artdevjs
// @homepageURL     https://gitlab.com/artdevjs/tampermonkey-userscripts
// @supportURL      https://gitlab.com/artdevjs/tampermonkey-userscripts/issues
// @license         MIT
// @match           https://github.com/*
// @run-at          document-end
// @grant           none
// @icon            https://raw.githubusercontent.com/sairam/gitnotify-chrome-ext/master/icons/gitnotify-48.png
// ==/UserScript==

(() => {
  const API_URL = 'https://gitnotify.com';
  const API_PATH = '/typeahead/tz?offset=5.5'; // eslint-disable-line

  const getPermalink = (repoName, tree) => `${API_URL}?src=github&repo=${repoName}&tree=${tree}`;

  const getHTML = (repoName, tree) => {
    const href = getPermalink(repoName, tree);

    return `<li><a href="${href}" target="_blank" id="gitnotify" class="btn btn-sm" aria-label="Get Notifications for this repository">
        <svg class="octicon octicon-star v-align-text-bottom" viewBox="0 0 14 16" version="1.1" width="14" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M 9.5 3 L 8 4.5 L 11.5 8 L 8 11.5 L 9.5 13 L 14 8 L 9.5 3 Z M 4.5 3 L 0 8 L 4.5 13 L 6 11.5 L 2.5 8 L 6 4.5 L 4.5 3 Z"/></svg> GitNotify
      </a><li>`;
  };

  const isRepo = (uri) => {
    const repoURI = uri.split('/');

    return (
      repoURI.length === 2
      || repoURI[2] === 'tree'
      || repoURI[2] === 'blob'
      || repoURI[2] === 'graphs'
      || repoURI[2] === 'pulse'
      || repoURI[2] === 'wiki'
      || repoURI[2] === 'projects'
      || repoURI[2] === 'pulls'
      || repoURI[2] === 'issues'
      || repoURI[2] === 'releases'
      || repoURI[2] === 'tags'
    );
  };

  const getTree = (uri) => {
    const repoURI = uri.split('/');

    if (repoURI[2] === 'tree' || repoURI[2] === 'blob') {
      return repoURI[3];
    }

    return null;
  };

  const getRepoInfoURI = (uri) => {
    const repoURI = uri.split('/');

    return `${repoURI[0]}/${repoURI[1]}`;
  };

  // const checkStatus = (response) => {
  //   if (response.status >= 200 && response.status < 300) {
  //     return response;
  //   }

  //   throw Error(`Server returned a bad status: ${response.status}`);
  // };

  // const parseJSON = (response) => {
  //   if (response) {
  //     return response.json();
  //   }

  //   throw Error('Could not parse JSON');
  // };

  // eslint-disable-next-line
  // const getAPIData = (uri, callback) => {
  //   const request = new Request(API_URL + uri, {
  //     headers: new Headers({
  //       'User-Agent': 'github/chrome-ext',
  //     }),
  //   });

  //   fetch(request)
  //     .then(checkStatus)
  //     .then(parseJSON)
  //     .then(callback)
  //     .catch(e => console.error(e));
  // };

  const run = () => {
    const repoURI = window.location.pathname.substring(1);

    if (isRepo(repoURI)) {
      const ns = document.querySelector('ul.pagehead-actions');
      const liElem = document.getElementById('gitnotify');

      if (ns && !liElem) {
        const htmlData = getHTML(getRepoInfoURI(repoURI), getTree(repoURI));

        ns.insertAdjacentHTML('beforeend', htmlData);
      }

      //  TODO: update the current gitnotify url to see if this is being tracked
      // check if user is logged in, if so, add to track
      // getAPIData(API_PATH, function (data) {
      //   if (data) {
      //     console.log(data)
      //   }
      // })
    }
  };

  run();
  document.addEventListener('pjax:end', run, false);
})();
