// ==UserScript==
// @name            CodeCopy
// @description     Because copy to clipboard buttons should exist on every code snippet
// @version         0.1.0
// @author          artdev
// @namespace       github.com/zenorocha/codecopy
// @homepageURL     https://github.com/zenorocha/codecopy
// @supportURL      https://gitlab.com/artdevjs/tampermonkey-userscripts/issues
// @license         MIT
// @include         https://developer.mozilla.org/*
// @include         https://medium.com/*
// @include         https://www.npmjs.com/*
// @include         https://github.com/*
// @include         https://gist.github.com/*
// @include         http://stackexchange.com/*
// @include         https://stackexchange.com/*
// @include         http://*.stackexchange.com/*
// @include         https://*.stackexchange.com/*
// @include         https://serverfault.com/*
// @include         https://superuser.com/*
// @include         https://askubuntu.com/*
// @include         http://stackoverflow.com/*
// @include         https://stackoverflow.com/*
// @run-at          document-end
// @grant           GM_addStyle
// @icon            https://raw.githubusercontent.com/zenorocha/codecopy/master/dist/images/icon-48.png
// ==/UserScript==

/* eslint-disable brace-style, no-unused-vars */

GM_addStyle('@keyframes tooltip-appear{0%{opacity:0}to{opacity:1}}.codecopy{overflow:visible}.codecopy,.codecopy .tooltipped{position:relative}.codecopy .tooltipped::after,.codecopy .tooltipped::before{position:absolute;display:none;pointer-events:none;opacity:0}.codecopy .tooltipped::after{padding:5px 8px;font:11px/1.5 -apple-system,BlinkMacSystemFont,"Segoe UI",Helvetica,Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";-webkit-font-smoothing:subpixel-antialiased;color:#fff;text-align:center;text-decoration:none;text-shadow:none;text-transform:none;letter-spacing:normal;word-wrap:break-word;white-space:pre;content:attr(aria-label);background:rgba(0,0,0,.8);border-radius:3px;z-index:1000000}.codecopy .tooltipped::before{z-index:1000001;width:0;height:0;color:rgba(0,0,0,.8);content:"";border:5px solid transparent}.codecopy .tooltipped:active::after,.codecopy .tooltipped:active::before,.codecopy .tooltipped:focus::after,.codecopy .tooltipped:focus::before,.codecopy .tooltipped:hover::after,.codecopy .tooltipped:hover::before{display:inline-block;text-decoration:none;animation-name:tooltip-appear;animation-duration:.1s;animation-fill-mode:forwards;animation-timing-function:ease-in;animation-delay:.4s}.codecopy .tooltipped-no-delay:active::after,.codecopy .tooltipped-no-delay:active::before,.codecopy .tooltipped-no-delay:focus::after,.codecopy .tooltipped-no-delay:focus::before,.codecopy .tooltipped-no-delay:hover::after,.codecopy .tooltipped-no-delay:hover::before{opacity:1;animation:none}.codecopy .tooltipped-multiline:active::after,.codecopy .tooltipped-multiline:focus::after,.codecopy .tooltipped-multiline:hover::after{display:table-cell}.codecopy .tooltipped-s::after{top:100%;right:50%;margin-top:5px}.codecopy .tooltipped-se::after{top:100%;margin-top:5px}.codecopy .tooltipped-sw::after{top:100%;right:50%;margin-top:5px}.codecopy .tooltipped-s::before,.codecopy .tooltipped-se::before,.codecopy .tooltipped-sw::before{top:auto;right:50%;bottom:-5px;margin-right:-5px;border-bottom-color:rgba(0,0,0,.8)}.codecopy .tooltipped-se::after{right:auto;left:50%;margin-left:-15px}.codecopy .tooltipped-sw::after{margin-right:-15px}.codecopy .tooltipped-n::after{right:50%;bottom:100%;margin-bottom:5px}.codecopy .tooltipped-ne::after{bottom:100%;margin-bottom:5px}.codecopy .tooltipped-nw::after{right:50%;bottom:100%;margin-bottom:5px}.codecopy .tooltipped-n::before,.codecopy .tooltipped-ne::before,.codecopy .tooltipped-nw::before{top:-5px;right:50%;bottom:auto;margin-right:-5px;border-top-color:rgba(0,0,0,.8)}.codecopy .tooltipped-ne::after{right:auto;left:50%;margin-left:-15px}.codecopy .tooltipped-nw::after{margin-right:-15px}.codecopy .tooltipped-n::after,.codecopy .tooltipped-s::after{transform:translateX(50%)}.codecopy .tooltipped-w::after{right:100%;bottom:50%;margin-right:5px;transform:translateY(50%)}.codecopy .tooltipped-w::before{top:50%;bottom:50%;left:-5px;margin-top:-5px;border-left-color:rgba(0,0,0,.8)}.codecopy .tooltipped-e::after{bottom:50%;left:100%;margin-left:5px;transform:translateY(50%)}.codecopy .tooltipped-e::before{top:50%;right:-5px;bottom:50%;margin-top:-5px;border-right-color:rgba(0,0,0,.8)}.codecopy .tooltipped-multiline::after{width:max-content;max-width:250px;word-break:break-word;word-wrap:normal;white-space:pre-line;border-collapse:separate}.codecopy .tooltipped-multiline.tooltipped-n::after,.codecopy .tooltipped-multiline.tooltipped-s::after{right:auto;left:50%;transform:translateX(-50%)}.codecopy .tooltipped-multiline.tooltipped-e::after,.codecopy .tooltipped-multiline.tooltipped-w::after{right:100%}@media screen and (min-width:0 ){.codecopy .tooltipped-multiline::after{width:250px}}.codecopy .tooltipped-sticky::after,.codecopy .tooltipped-sticky::before{display:inline-block}.codecopy .tooltipped-sticky.tooltipped-multiline::after{display:table-cell}@media only screen and (-webkit-min-device-pixel-ratio:2),only screen and (min--moz-device-pixel-ratio:2),only screen and (-moz-min-device-pixel-ratio:2),only screen and (-o-min-device-pixel-ratio:2/1),only screen and (min-device-pixel-ratio:2),only screen and (min-resolution:192dpi),only screen and (min-resolution:2dppx){.codecopy .tooltipped-w::after{margin-right:4.5px}}.codecopy .btn{position:relative;padding:6px 12px;font-size:14px;line-height:20px}.codecopy .btn,.codecopy .codecopy-btn{display:inline-block;font-weight:600;white-space:nowrap;vertical-align:middle;cursor:pointer;user-select:none;background-repeat:repeat-x;background-position:-1px -1px;background-size:110% 110%;border:1px solid rgba(27,31,35,.2);border-radius:.25em;appearance:none;color:#24292e;background-color:#eff3f6;background-image:linear-gradient(-180deg,#fafbfc 0%,#eff3f6 90%)}.codecopy .btn i,.codecopy .codecopy-btn i{font-style:normal;font-weight:500;opacity:.6}.codecopy .btn .octicon,.codecopy .codecopy-btn .octicon{vertical-align:text-top}.codecopy .btn .counter,.codecopy .codecopy-btn .counter{color:#586069;text-shadow:none;background-color:rgba(27,31,35,.1)}.codecopy .btn:hover,.codecopy .codecopy-btn:hover{text-decoration:none;background-repeat:repeat-x}.codecopy .btn:focus,.codecopy .codecopy-btn:focus{outline:0}.codecopy .btn.disabled,.codecopy .btn:disabled,.codecopy .codecopy-btn:disabled,.codecopy .disabled.codecopy-btn{cursor:default;background-position:0 0}.codecopy .btn.focus,.codecopy .btn:focus,.codecopy .codecopy-btn:focus,.codecopy .focus.codecopy-btn{box-shadow:0 0 0 .2em rgba(3,102,214,.3)}.codecopy .btn.hover,.codecopy .btn:hover,.codecopy .codecopy-btn:hover,.codecopy .hover.codecopy-btn{background-color:#e6ebf1;background-image:linear-gradient(-180deg,#f0f3f6 0%,#e6ebf1 90%);background-position:0 -.5em;border-color:rgba(27,31,35,.35)}.codecopy .btn.selected,.codecopy .btn:active,.codecopy .codecopy-btn:active,.codecopy .selected.codecopy-btn{background-color:#e9ecef;background-image:none;border-color:rgba(27,31,35,.35);box-shadow:inset 0 .15em .3em rgba(27,31,35,.15)}.codecopy .btn.disabled,.codecopy .btn:disabled,.codecopy .codecopy-btn:disabled,.codecopy .disabled.codecopy-btn{color:rgba(36,41,46,.4);background-color:#eff3f6;background-image:none;border-color:rgba(27,31,35,.2);box-shadow:none}.codecopy .btn-primary{color:#fff;background-color:#28a745;background-image:linear-gradient(-180deg,#34d058 0%,#28a745 90%)}.codecopy .btn-primary.focus,.codecopy .btn-primary:focus{box-shadow:0 0 0 .2em rgba(52,208,88,.3)}.codecopy .btn-primary.hover,.codecopy .btn-primary:hover{background-color:#269f42;background-image:linear-gradient(-180deg,#2fcb53 0%,#269f42 90%);background-position:0 -.5em;border-color:rgba(27,31,35,.5)}.codecopy .btn-primary.selected,.codecopy .btn-primary:active{background-color:#279f43;background-image:none;border-color:rgba(27,31,35,.5);box-shadow:inset 0 .15em .3em rgba(27,31,35,.15)}.codecopy .btn-primary.disabled,.codecopy .btn-primary:disabled{color:rgba(255,255,255,.75);background-color:#94d3a2;background-image:none;border-color:rgba(27,31,35,.2);box-shadow:none}.codecopy .btn-primary .counter{color:#29b249;background-color:#fff}.codecopy .btn-purple{color:#fff;background-color:#643ab0;background-image:linear-gradient(-180deg,#7e55c7 0%,#643ab0 90%)}.codecopy .btn-purple.focus,.codecopy .btn-purple:focus{box-shadow:0 0 0 .2em rgba(126,85,199,.3)}.codecopy .btn-purple.hover,.codecopy .btn-purple:hover{background-color:#5f37a8;background-image:linear-gradient(-180deg,#784ec5 0%,#5f37a8 90%);background-position:0 -.5em;border-color:rgba(27,31,35,.5)}.codecopy .btn-purple.selected,.codecopy .btn-purple:active{background-color:#613ca4;background-image:none;border-color:rgba(27,31,35,.5);box-shadow:inset 0 .15em .3em rgba(27,31,35,.15)}.codecopy .btn-purple.disabled,.codecopy .btn-purple:disabled{color:rgba(255,255,255,.75);background-color:#b19cd7;background-image:none;border-color:rgba(27,31,35,.2);box-shadow:none}.codecopy .btn-purple .counter{color:#683cb8;background-color:#fff}.codecopy .btn-blue{color:#fff;background-color:#0361cc;background-image:linear-gradient(-180deg,#0679fc 0%,#0361cc 90%)}.codecopy .btn-blue.focus,.codecopy .btn-blue:focus{box-shadow:0 0 0 .2em rgba(6,121,252,.3)}.codecopy .btn-blue.hover,.codecopy .btn-blue:hover{background-color:#035cc2;background-image:linear-gradient(-180deg,#0374f4 0%,#035cc2 90%);background-position:0 -.5em;border-color:rgba(27,31,35,.5)}.codecopy .btn-blue.selected,.codecopy .btn-blue:active{background-color:#045cc1;background-image:none;border-color:rgba(27,31,35,.5);box-shadow:inset 0 .15em .3em rgba(27,31,35,.15)}.codecopy .btn-blue.disabled,.codecopy .btn-blue:disabled{color:rgba(255,255,255,.75);background-color:#81b0e5;background-image:none;border-color:rgba(27,31,35,.2);box-shadow:none}.codecopy .btn-blue .counter{color:#0366d6;background-color:#fff}.codecopy .btn-danger{color:#cb2431;background-color:#fafbfc;background-image:linear-gradient(-180deg,#fafbfc 0%,#eff3f6 90%)}.codecopy .btn-danger:focus{box-shadow:0 0 0 .2em rgba(203,36,49,.3)}.codecopy .btn-danger:hover{color:#fff;background-color:#cb2431;background-image:linear-gradient(-180deg,#de4450 0%,#cb2431 90%);border-color:rgba(27,31,35,.5)}.codecopy .btn-danger:hover .counter{color:#fff}.codecopy .btn-danger.selected,.codecopy .btn-danger:active{color:#fff;background-color:#b5202c;background-image:none;border-color:rgba(27,31,35,.5);box-shadow:inset 0 .15em .3em rgba(27,31,35,.15)}.codecopy .btn-danger.disabled,.codecopy .btn-danger:disabled{color:rgba(203,36,49,.4);background-color:#eff3f6;background-image:none;border-color:rgba(27,31,35,.2);box-shadow:none}.codecopy .btn-outline{background-image:none}.codecopy .btn-outline .counter{background-color:rgba(0,0,0,.07)}.codecopy .btn-outline.selected,.codecopy .btn-outline:active,.codecopy .btn-outline:hover{color:#fff;background-color:#0366d6;background-image:none;border-color:#0366d6}.codecopy .btn-outline,.codecopy .btn-outline.selected .counter,.codecopy .btn-outline:active .counter,.codecopy .btn-outline:hover .counter{color:#0366d6;background-color:#fff}.codecopy .btn-outline:focus{border-color:#0366d6;box-shadow:0 0 0 .2em rgba(3,102,214,.3)}.codecopy .btn-outline.disabled,.codecopy .btn-outline:disabled{color:rgba(27,31,35,.3);background-color:#fff;border-color:rgba(0,0,0,.15);box-shadow:none}.codecopy .btn-with-count{float:left;border-top-right-radius:0;border-bottom-right-radius:0}.codecopy .btn-sm{padding:3px 10px}.codecopy .btn-sm,.codecopy .codecopy-btn{font-size:12px;line-height:20px}.codecopy .btn-large{padding:.75em 1.25em;font-size:inherit;border-radius:6px}.codecopy .hidden-text-expander{display:block}.codecopy .hidden-text-expander.inline{position:relative;top:-1px;display:inline-block;margin-left:5px;line-height:0}.codecopy .ellipsis-expander,.codecopy .hidden-text-expander a{display:inline-block;height:12px;padding:0 5px 5px;font-size:12px;font-weight:600;line-height:6px;color:#444d56;text-decoration:none;vertical-align:middle;background:#dfe2e5;border:0;border-radius:1px}.codecopy .ellipsis-expander:hover,.codecopy .hidden-text-expander a:hover{text-decoration:none;background-color:#c6cbd1}.codecopy .ellipsis-expander:active,.codecopy .hidden-text-expander a:active{color:#fff;background-color:#2188ff}.codecopy .social-count{float:left;padding:3px 10px;font-size:12px;font-weight:600;line-height:20px;color:#24292e;vertical-align:middle;background-color:#fff;border:1px solid rgba(27,31,35,.2);border-left:0;border-top-right-radius:3px;border-bottom-right-radius:3px}.codecopy .social-count:active,.codecopy .social-count:hover{text-decoration:none}.codecopy .social-count:hover{color:#0366d6;cursor:pointer}.codecopy .btn-block{display:block;width:100%;text-align:center}.codecopy .btn-link{display:inline-block;padding:0;font-size:inherit;color:#0366d6;text-decoration:none;white-space:nowrap;cursor:pointer;user-select:none;background-color:transparent;border:0;appearance:none}.codecopy .btn-link:hover{text-decoration:underline}.codecopy .btn-link:disabled,.codecopy .btn-link:disabled:hover{color:rgba(88,96,105,.5);cursor:default}.codecopy .BtnGroup{display:inline-block;vertical-align:middle}.codecopy .BtnGroup::before{display:table;content:""}.codecopy .BtnGroup::after{display:table;clear:both;content:""}.codecopy .BtnGroup+.BtnGroup,.codecopy .BtnGroup+.btn,.codecopy .BtnGroup+.codecopy-btn{margin-left:5px}.codecopy .BtnGroup-item{position:relative;float:left;border-right-width:0;border-radius:0}.codecopy .BtnGroup-item:first-child{border-top-left-radius:3px;border-bottom-left-radius:3px}.codecopy .BtnGroup-item:last-child{border-right-width:1px;border-top-right-radius:3px;border-bottom-right-radius:3px}.codecopy .BtnGroup-item.selected,.codecopy .BtnGroup-item:active,.codecopy .BtnGroup-item:focus,.codecopy .BtnGroup-item:hover{border-right-width:1px}.codecopy .BtnGroup-item.selected+.BtnGroup-form .BtnGroup-item,.codecopy .BtnGroup-item.selected+.BtnGroup-item,.codecopy .BtnGroup-item:active+.BtnGroup-form .BtnGroup-item,.codecopy .BtnGroup-item:active+.BtnGroup-item,.codecopy .BtnGroup-item:focus+.BtnGroup-form .BtnGroup-item,.codecopy .BtnGroup-item:focus+.BtnGroup-item,.codecopy .BtnGroup-item:hover+.BtnGroup-form .BtnGroup-item,.codecopy .BtnGroup-item:hover+.BtnGroup-item{border-left-width:0}.codecopy .BtnGroup-form{float:left}.codecopy .BtnGroup-form:first-child .BtnGroup-item{border-top-left-radius:3px;border-bottom-left-radius:3px}.codecopy .BtnGroup-form:last-child .BtnGroup-item{border-right-width:1px;border-top-right-radius:3px;border-bottom-right-radius:3px}.codecopy .BtnGroup-form .BtnGroup-item{border-right-width:0;border-radius:0}.codecopy .BtnGroup-form.selected .BtnGroup-item,.codecopy .BtnGroup-form:active .BtnGroup-item,.codecopy .BtnGroup-form:focus .BtnGroup-item,.codecopy .BtnGroup-form:hover .BtnGroup-item{border-right-width:1px}.codecopy .BtnGroup-form.selected+.BtnGroup-form .BtnGroup-item,.codecopy .BtnGroup-form.selected+.BtnGroup-item,.codecopy .BtnGroup-form:active+.BtnGroup-form .BtnGroup-item,.codecopy .BtnGroup-form:active+.BtnGroup-item,.codecopy .BtnGroup-form:focus+.BtnGroup-form .BtnGroup-item,.codecopy .BtnGroup-form:focus+.BtnGroup-item,.codecopy .BtnGroup-form:hover+.BtnGroup-form .BtnGroup-item,.codecopy .BtnGroup-form:hover+.BtnGroup-item{border-left-width:0}.codecopy .codecopy-btn{box-shadow:none;min-height:initial;transition:opacity .3s ease-in-out;opacity:0;position:absolute;padding:2px 6px;right:0;top:0;z-index:1}.codecopy .codecopy-btn .codecopy-btn-icon{margin-top:-3px;position:relative;top:3px;padding:0;vertical-align:initial;min-height:initial}.codecopy .codecopy-btn:focus,.codecopy .codecopy-btn:hover{box-shadow:none}.codecopy:hover .codecopy-btn{opacity:1}.codecopy.codecopy-lg .codecopy-btn{padding:3px 6px;right:5px;top:5px}');
(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){let a=typeof require==='function'&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);let f=new Error("Cannot find module '"+o+"'");throw f.code='MODULE_NOT_FOUND',f}let l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){let n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}let i=typeof require==='function'&&require;for(let o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
  (function (global, factory) {
    if (typeof define === 'function' && define.amd) {
      define(['module', 'select'], factory);
    } else if (typeof exports !== 'undefined') {
      factory(module, require('select'));
    } else {
      let mod = {
        exports: {}
      };
      factory(mod, global.select);
      global.clipboardAction = mod.exports;
    }
  })(this, function (module, _select) {
    'use strict';

    let _select2 = _interopRequireDefault(_select);

    function _interopRequireDefault(obj) {
      return obj && obj.__esModule ? obj : {
        default: obj
      };
    }

    let _typeof = typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol' ? function (obj) {
      return typeof obj;
    } : function (obj) {
      return obj && typeof Symbol === 'function' && obj.constructor === Symbol && obj !== Symbol.prototype ? 'symbol' : typeof obj;
    };

    function _classCallCheck(instance, Constructor) {
      if (!(instance instanceof Constructor)) {
        throw new TypeError('Cannot call a class as a function');
      }
    }

    let _createClass = function () {
      function defineProperties(target, props) {
        for (let i = 0; i < props.length; i++) {
          let descriptor = props[i];
          descriptor.enumerable = descriptor.enumerable || false;
          descriptor.configurable = true;
          if ('value' in descriptor) descriptor.writable = true;
          Object.defineProperty(target, descriptor.key, descriptor);
        }
      }

      return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);
        if (staticProps) defineProperties(Constructor, staticProps);
        return Constructor;
      };
    }();

    let ClipboardAction = function () {
      /**
      * @param {Object} options
      */
      function ClipboardAction(options) {
        _classCallCheck(this, ClipboardAction);

        this.resolveOptions(options);
        this.initSelection();
      }

      /**
      * Defines base properties passed from constructor.
      * @param {Object} options
      */


      _createClass(ClipboardAction, [{
        key: 'resolveOptions',
        value: function resolveOptions() {
          let options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

          this.action = options.action;
          this.emitter = options.emitter;
          this.target = options.target;
          this.text = options.text;
          this.trigger = options.trigger;

          this.selectedText = '';
        }
      }, {
        key: 'initSelection',
        value: function initSelection() {
          if (this.text) {
            this.selectFake();
          } else if (this.target) {
            this.selectTarget();
          }
        }
      }, {
        key: 'selectFake',
        value: function selectFake() {
          let _this = this;

          let isRTL = document.documentElement.getAttribute('dir') === 'rtl';

          this.removeFake();

          this.fakeHandlerCallback = function () {
            return _this.removeFake();
          };
          this.fakeHandler = document.body.addEventListener('click', this.fakeHandlerCallback) || true;

          this.fakeElem = document.createElement('textarea');
          // Prevent zooming on iOS
          this.fakeElem.style.fontSize = '12pt';
          // Reset box model
          this.fakeElem.style.border = '0';
          this.fakeElem.style.padding = '0';
          this.fakeElem.style.margin = '0';
          // Move element out of screen horizontally
          this.fakeElem.style.position = 'absolute';
          this.fakeElem.style[isRTL ? 'right' : 'left'] = '-9999px';
          // Move element to the same position vertically
          let yPosition = window.pageYOffset || document.documentElement.scrollTop;
          this.fakeElem.style.top = yPosition + 'px';

          this.fakeElem.setAttribute('readonly', '');
          this.fakeElem.value = this.text;

          document.body.appendChild(this.fakeElem);

          this.selectedText = (0, _select2.default)(this.fakeElem);
          this.copyText();
        }
      }, {
        key: 'removeFake',
        value: function removeFake() {
          if (this.fakeHandler) {
            document.body.removeEventListener('click', this.fakeHandlerCallback);
            this.fakeHandler = null;
            this.fakeHandlerCallback = null;
          }

          if (this.fakeElem) {
            document.body.removeChild(this.fakeElem);
            this.fakeElem = null;
          }
        }
      }, {
        key: 'selectTarget',
        value: function selectTarget() {
          this.selectedText = (0, _select2.default)(this.target);
          this.copyText();
        }
      }, {
        key: 'copyText',
        value: function copyText() {
          let succeeded = void 0;

          try {
            succeeded = document.execCommand(this.action);
          } catch (err) {
            succeeded = false;
          }

          this.handleResult(succeeded);
        }
      }, {
        key: 'handleResult',
        value: function handleResult(succeeded) {
          this.emitter.emit(succeeded ? 'success' : 'error', {
            action: this.action,
            text: this.selectedText,
            trigger: this.trigger,
            clearSelection: this.clearSelection.bind(this)
          });
        }
      }, {
        key: 'clearSelection',
        value: function clearSelection() {
          if (this.target) {
            this.target.blur();
          }

          window.getSelection().removeAllRanges();
        }
      }, {
        key: 'destroy',
        value: function destroy() {
          this.removeFake();
        }
      }, {
        key: 'action',
        set: function set() {
          let action = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'copy';

          this._action = action;

          if (this._action !== 'copy' && this._action !== 'cut') {
            throw new Error('Invalid "action" value, use either "copy" or "cut"');
          }
        },
        get: function get() {
          return this._action;
        }
      }, {
        key: 'target',
        set: function set(target) {
          if (target !== undefined) {
            if (target && (typeof target === 'undefined' ? 'undefined' : _typeof(target)) === 'object' && target.nodeType === 1) {
              if (this.action === 'copy' && target.hasAttribute('disabled')) {
                throw new Error('Invalid "target" attribute. Please use "readonly" instead of "disabled" attribute');
              }

              if (this.action === 'cut' && (target.hasAttribute('readonly') || target.hasAttribute('disabled'))) {
                throw new Error('Invalid "target" attribute. You can\'t cut text from elements with "readonly" or "disabled" attributes');
              }

              this._target = target;
            } else {
              throw new Error('Invalid "target" value, use a valid Element');
            }
          }
        },
        get: function get() {
          return this._target;
        }
      }]);

      return ClipboardAction;
    }();

    module.exports = ClipboardAction;
  });
},{'select':7}],2:[function(require,module,exports){
  (function (global, factory) {
    if (typeof define === 'function' && define.amd) {
      define(['module', './clipboard-action', 'tiny-emitter', 'good-listener'], factory);
    } else if (typeof exports !== 'undefined') {
      factory(module, require('./clipboard-action'), require('tiny-emitter'), require('good-listener'));
    } else {
      let mod = {
        exports: {}
      };
      factory(mod, global.clipboardAction, global.tinyEmitter, global.goodListener);
      global.clipboard = mod.exports;
    }
  })(this, function (module, _clipboardAction, _tinyEmitter, _goodListener) {
    'use strict';

    let _clipboardAction2 = _interopRequireDefault(_clipboardAction);

    let _tinyEmitter2 = _interopRequireDefault(_tinyEmitter);

    let _goodListener2 = _interopRequireDefault(_goodListener);

    function _interopRequireDefault(obj) {
      return obj && obj.__esModule ? obj : {
        default: obj
      };
    }

    function _classCallCheck(instance, Constructor) {
      if (!(instance instanceof Constructor)) {
        throw new TypeError('Cannot call a class as a function');
      }
    }

    let _createClass = function () {
      function defineProperties(target, props) {
        for (let i = 0; i < props.length; i++) {
          let descriptor = props[i];
          descriptor.enumerable = descriptor.enumerable || false;
          descriptor.configurable = true;
          if ('value' in descriptor) descriptor.writable = true;
          Object.defineProperty(target, descriptor.key, descriptor);
        }
      }

      return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);
        if (staticProps) defineProperties(Constructor, staticProps);
        return Constructor;
      };
    }();

    function _possibleConstructorReturn(self, call) {
      if (!self) {
        throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
      }

      return call && (typeof call === 'object' || typeof call === 'function') ? call : self;
    }

    function _inherits(subClass, superClass) {
      if (typeof superClass !== 'function' && superClass !== null) {
        throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass);
      }

      subClass.prototype = Object.create(superClass && superClass.prototype, {
        constructor: {
          value: subClass,
          enumerable: false,
          writable: true,
          configurable: true
        }
      });
      if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    let Clipboard = function (_Emitter) {
      _inherits(Clipboard, _Emitter);

      /**
      * @param {String|HTMLElement|HTMLCollection|NodeList} trigger
      * @param {Object} options
      */
      function Clipboard(trigger, options) {
        _classCallCheck(this, Clipboard);

        let _this = _possibleConstructorReturn(this, (Clipboard.__proto__ || Object.getPrototypeOf(Clipboard)).call(this));

        _this.resolveOptions(options);
        _this.listenClick(trigger);
        return _this;
      }

      /**
      * Defines if attributes would be resolved using internal setter functions
      * or custom functions that were passed in the constructor.
      * @param {Object} options
      */


      _createClass(Clipboard, [{
        key: 'resolveOptions',
        value: function resolveOptions() {
          let options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

          this.action = typeof options.action === 'function' ? options.action : this.defaultAction;
          this.target = typeof options.target === 'function' ? options.target : this.defaultTarget;
          this.text = typeof options.text === 'function' ? options.text : this.defaultText;
        }
      }, {
        key: 'listenClick',
        value: function listenClick(trigger) {
          let _this2 = this;

          this.listener = (0, _goodListener2.default)(trigger, 'click', function (e) {
            return _this2.onClick(e);
          });
        }
      }, {
        key: 'onClick',
        value: function onClick(e) {
          let trigger = e.delegateTarget || e.currentTarget;

          if (this.clipboardAction) {
            this.clipboardAction = null;
          }

          this.clipboardAction = new _clipboardAction2.default({
            action: this.action(trigger),
            target: this.target(trigger),
            text: this.text(trigger),
            trigger: trigger,
            emitter: this
          });
        }
      }, {
        key: 'defaultAction',
        value: function defaultAction(trigger) {
          return getAttributeValue('action', trigger);
        }
      }, {
        key: 'defaultTarget',
        value: function defaultTarget(trigger) {
          let selector = getAttributeValue('target', trigger);

          if (selector) {
            return document.querySelector(selector);
          }
        }
      }, {
        key: 'defaultText',
        value: function defaultText(trigger) {
          return getAttributeValue('text', trigger);
        }
      }, {
        key: 'destroy',
        value: function destroy() {
          this.listener.destroy();

          if (this.clipboardAction) {
            this.clipboardAction.destroy();
            this.clipboardAction = null;
          }
        }
      }], [{
        key: 'isSupported',
        value: function isSupported() {
          let action = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : ['copy', 'cut'];

          let actions = typeof action === 'string' ? [action] : action;
          let support = !!document.queryCommandSupported;

          actions.forEach(function (action) {
            support = support && !!document.queryCommandSupported(action);
          });

          return support;
        }
      }]);

      return Clipboard;
    }(_tinyEmitter2.default);

    /**
    * Helper function to retrieve attribute value.
    * @param {String} suffix
    * @param {Element} element
    */
    function getAttributeValue(suffix, element) {
      let attribute = 'data-clipboard-' + suffix;

      if (!element.hasAttribute(attribute)) {
        return;
      }

      return element.getAttribute(attribute);
    }

    module.exports = Clipboard;
  });
},{'./clipboard-action':1,'good-listener':6,'tiny-emitter':8}],3:[function(require,module,exports){
  let DOCUMENT_NODE_TYPE = 9;

  /**
  * A polyfill for Element.matches()
  */
  if (typeof Element !== 'undefined' && !Element.prototype.matches) {
    let proto = Element.prototype;

    proto.matches = proto.matchesSelector ||
    proto.mozMatchesSelector ||
    proto.msMatchesSelector ||
    proto.oMatchesSelector ||
    proto.webkitMatchesSelector;
  }

  /**
  * Finds the closest parent that matches a selector.
  *
  * @param {Element} element
  * @param {String} selector
  * @return {Function}
  */
  function closest (element, selector) {
    while (element && element.nodeType !== DOCUMENT_NODE_TYPE) {
      if (element.matches(selector)) return element;
      element = element.parentNode;
    }
  }

  module.exports = closest;

},{}],4:[function(require,module,exports){
  let closest = require('./closest');

  /**
  * Delegates event to a selector.
  *
  * @param {Element} element
  * @param {String} selector
  * @param {String} type
  * @param {Function} callback
  * @param {Boolean} useCapture
  * @return {Object}
  */
  function delegate(element, selector, type, callback, useCapture) {
    let listenerFn = listener.apply(this, arguments);

    element.addEventListener(type, listenerFn, useCapture);

    return {
      destroy: function() {
        element.removeEventListener(type, listenerFn, useCapture);
      }
    }
  }

  /**
  * Finds closest match and invokes callback.
  *
  * @param {Element} element
  * @param {String} selector
  * @param {String} type
  * @param {Function} callback
  * @return {Function}
  */
  function listener(element, selector, type, callback) {
    return function(e) {
      e.delegateTarget = closest(e.target, selector);

      if (e.delegateTarget) {
        callback.call(element, e);
      }
    }
  }

  module.exports = delegate;

},{'./closest':3}],5:[function(require,module,exports){
  /**
  * Check if argument is a HTML element.
  *
  * @param {Object} value
  * @return {Boolean}
  */
  exports.node = function(value) {
    return value !== undefined
    && value instanceof HTMLElement
    && value.nodeType === 1;
  };

  /**
  * Check if argument is a list of HTML elements.
  *
  * @param {Object} value
  * @return {Boolean}
  */
  exports.nodeList = function(value) {
    let type = Object.prototype.toString.call(value);

    return value !== undefined
    && (type === '[object NodeList]' || type === '[object HTMLCollection]')
    && ('length' in value)
    && (value.length === 0 || exports.node(value[0]));
  };

  /**
  * Check if argument is a string.
  *
  * @param {Object} value
  * @return {Boolean}
  */
  exports.string = function(value) {
    return typeof value === 'string'
    || value instanceof String;
  };

  /**
  * Check if argument is a function.
  *
  * @param {Object} value
  * @return {Boolean}
  */
  exports.fn = function(value) {
    let type = Object.prototype.toString.call(value);

    return type === '[object Function]';
  };

},{}],6:[function(require,module,exports){
  let is = require('./is');
  let delegate = require('delegate');

  /**
  * Validates all params and calls the right
  * listener function based on its target type.
  *
  * @param {String|HTMLElement|HTMLCollection|NodeList} target
  * @param {String} type
  * @param {Function} callback
  * @return {Object}
  */
  function listen(target, type, callback) {
    if (!target && !type && !callback) {
      throw new Error('Missing required arguments');
    }

    if (!is.string(type)) {
      throw new TypeError('Second argument must be a String');
    }

    if (!is.fn(callback)) {
      throw new TypeError('Third argument must be a Function');
    }

    if (is.node(target)) {
      return listenNode(target, type, callback);
    }
    else if (is.nodeList(target)) {
      return listenNodeList(target, type, callback);
    }
    else if (is.string(target)) {
      return listenSelector(target, type, callback);
    }

    throw new TypeError('First argument must be a String, HTMLElement, HTMLCollection, or NodeList');
  }

  /**
  * Adds an event listener to a HTML element
  * and returns a remove listener function.
  *
  * @param {HTMLElement} node
  * @param {String} type
  * @param {Function} callback
  * @return {Object}
  */
  function listenNode(node, type, callback) {
    node.addEventListener(type, callback);

    return {
      destroy: function() {
        node.removeEventListener(type, callback);
      }
    }
  }

  /**
  * Add an event listener to a list of HTML elements
  * and returns a remove listener function.
  *
  * @param {NodeList|HTMLCollection} nodeList
  * @param {String} type
  * @param {Function} callback
  * @return {Object}
  */
  function listenNodeList(nodeList, type, callback) {
    Array.prototype.forEach.call(nodeList, function(node) {
      node.addEventListener(type, callback);
    });

    return {
      destroy: function() {
        Array.prototype.forEach.call(nodeList, function(node) {
          node.removeEventListener(type, callback);
        });
      }
    }
  }

  /**
  * Add an event listener to a selector
  * and returns a remove listener function.
  *
  * @param {String} selector
  * @param {String} type
  * @param {Function} callback
  * @return {Object}
  */
  function listenSelector(selector, type, callback) {
    return delegate(document.body, selector, type, callback);
  }

  module.exports = listen;

},{'./is':5,'delegate':4}],7:[function(require,module,exports){
  function select(element) {
    let selectedText;

    if (element.nodeName === 'SELECT') {
      element.focus();

      selectedText = element.value;
    }
    else if (element.nodeName === 'INPUT' || element.nodeName === 'TEXTAREA') {
      let isReadOnly = element.hasAttribute('readonly');

      if (!isReadOnly) {
        element.setAttribute('readonly', '');
      }

      element.select();
      element.setSelectionRange(0, element.value.length);

      if (!isReadOnly) {
        element.removeAttribute('readonly');
      }

      selectedText = element.value;
    }
    else {
      if (element.hasAttribute('contenteditable')) {
        element.focus();
      }

      let selection = window.getSelection();
      let range = document.createRange();

      range.selectNodeContents(element);
      selection.removeAllRanges();
      selection.addRange(range);

      selectedText = selection.toString();
    }

    return selectedText;
  }

  module.exports = select;

},{}],8:[function(require,module,exports){
  function E () {
    // Keep this empty so it's easier to inherit from
    // (via https://github.com/lipsmack from https://github.com/scottcorgan/tiny-emitter/issues/3)
  }

  E.prototype = {
    on: function (name, callback, ctx) {
      let e = this.e || (this.e = {});

      (e[name] || (e[name] = [])).push({
        fn: callback,
        ctx: ctx
      });

      return this;
    },

    once: function (name, callback, ctx) {
      let self = this;
      function listener () {
        self.off(name, listener);
        callback.apply(ctx, arguments);
      };

      listener._ = callback
      return this.on(name, listener, ctx);
    },

    emit: function (name) {
      let data = [].slice.call(arguments, 1);
      let evtArr = ((this.e || (this.e = {}))[name] || []).slice();
      let i = 0;
      let len = evtArr.length;

      for (i; i < len; i++) {
        evtArr[i].fn.apply(evtArr[i].ctx, data);
      }

      return this;
    },

    off: function (name, callback) {
      let e = this.e || (this.e = {});
      let evts = e[name];
      let liveEvents = [];

      if (evts && callback) {
        for (let i = 0, len = evts.length; i < len; i++) {
          if (evts[i].fn !== callback && evts[i].fn._ !== callback)
          { liveEvents.push(evts[i]); }
        }
      }

      // Remove event from queue to prevent memory leak
      // Suggested by https://github.com/lazd
      // Ref: https://github.com/scottcorgan/tiny-emitter/commit/c6ebfaa9bc973b33d110a84a307742b7cf94c953#commitcomment-5024910

      (liveEvents.length)
        ? e[name] = liveEvents
        : delete e[name];

      return this;
    }
  };

  module.exports = E;

},{}],9:[function(require,module,exports){
  'use strict';

  let _clipboard = require('clipboard');

  let _clipboard2 = _interopRequireDefault(_clipboard);

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

  let snippets = document.querySelectorAll('pre');

  [].forEach.call(snippets, function (snippet) {
    let parent = snippet.parentNode;
    let wrapper = document.createElement('div');

    parent.replaceChild(wrapper, snippet);
    wrapper.appendChild(snippet);

    wrapper.classList.add('codecopy');
    wrapper.firstChild.insertAdjacentHTML('beforebegin', '<button class="codecopy-btn tooltipped tooltipped-s" aria-label="Copy to clipboard"><svg height="16" class="codecopy-btn-icon" viewBox="0 0 14 16" version="1.1" width="16" aria-hidden="true"><path fill-rule="evenodd" d="M2 13h4v1H2v-1zm5-6H2v1h5V7zm2 3V8l-3 3 3 3v-2h5v-2H9zM4.5 9H2v1h2.5V9zM2 12h2.5v-1H2v1zm9 1h1v2c-.02.28-.11.52-.3.7-.19.18-.42.28-.7.3H1c-.55 0-1-.45-1-1V4c0-.55.45-1 1-1h3c0-1.11.89-2 2-2 1.11 0 2 .89 2 2h3c.55 0 1 .45 1 1v5h-1V6H1v9h10v-2zM2 5h8c0-.55-.45-1-1-1H8c-.55 0-1-.45-1-1s-.45-1-1-1-1 .45-1 1-.45 1-1 1H3c-.55 0-1 .45-1 1z"></path></svg></button>');

    let sites = /^(github.com|gist.github.com|medium.com|www.npmjs.com|developer.mozilla.org)$/;

    if (sites.exec(document.location.hostname)) {
      wrapper.classList.add('codecopy-lg');
    }
  });

  let clipboard = new _clipboard2.default('.codecopy-btn', {
    target: function target(trigger) {
      return trigger.parentNode;
    }
  });

  clipboard.on('success', function (e) {
    e.clearSelection();

    showTooltip(e.trigger, 'Copied!');
  });

  let btns = document.querySelectorAll('.codecopy-btn');

  for (let i = 0; i < btns.length; i++) {
    btns[i].addEventListener('mouseleave', function (e) {
      showTooltip(e.target, 'Copy to clipboard');
      e.target.blur();
    });
  }

  function showTooltip(elem, msg) {
    elem.setAttribute('aria-label', msg);
  }

},{'clipboard':2}]},{},[9]);
