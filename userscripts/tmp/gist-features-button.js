// ==UserScript==
// @name               gist-features-button
// @namespace          https://github.com/cologler/
// @version            1.2
// @description        add 'Raw (Newest)' and 'copy' to gist.
// @author             cologler
// @match              https://gist.github.com/*/*
// @match              https://app.gistboxapp.com/*
// @grant              GM_setClipboard
// @grant              GM_notification
// @grant              GM_xmlhttpRequest
// @connect            gist.githubusercontent.com
// @require            https://greasyfork.org/scripts/31694-ondom/code/OnDom.js
// @icon               https://github.githubassets.com/pinned-octocat.svg
// ==/UserScript==

// this script was hosting on: https://greasyfork.org/zh-CN/scripts/29107

// just let type script work.
// eslint-disable-next-line brace-style
(function() { function require(){}; require('greasemonkey'); })();

(function() {
  'use strict';

  function copyCode(url) {
    GM_xmlhttpRequest({
      method: 'GET',
      url: url,
      onload: z => {
        if (z.status === 200) {
          GM_setClipboard(z.responseText);
          console.log('copied');
          // GM_notification({
          //   text: 'copyed!'
          // });
        } else {
          GM_notification({
            text: 'error code: ' + z.status
          });
        }
      }
    });
  }

  if (location.host === 'gist.github.com') {
    onDom('.file-actions', z => {
      function cloneButton() {
        const newBtn = z.cloneNode(true);
        newBtn.style.marginRight = '6px';
        z.parentElement.insertBefore(newBtn, z.nextSibling);
        return newBtn;
      }

      if (z.children[0].innerText === 'Raw') {
        const rawUrl = z.children[0].href;

        { // Copy
          const btn = cloneButton();
          const a = btn.children[0];
          a.addEventListener('click', () => {
            copyCode(rawUrl);
            return false;
          });
          a.removeAttribute('href');
          a.innerText = 'copy';
        }
      }
    });
  }
})();
