// ==UserScript==
// @name            reCaptcha Clicker Lite
// @description     This solve google recaptcha automatic after sec
// @version         0.1.0
// @author          artdev
// @namespace       https://gitlab.com/artdevjs
// @homepageURL     https://gitlab.com/artdevjs/tampermonkey-userscripts
// @supportURL      https://gitlab.com/artdevjs/tampermonkey-userscripts/issues
// @license         MIT
// @include         *
// @run-at          document-idle
// @grant           none
// @icon            https://thirtybees.com/wp-content/uploads/2017/05/reCAPTCHA_cleaned.png
// ==/UserScript==

(function() {
  'use strict';

  var clickCheck = setInterval(function() {
    if (document.querySelectorAll('.recaptcha-checkbox-checkmark').length > 0) {
      clearInterval(clickCheck);
      document.querySelector('.recaptcha-checkbox-checkmark').click();
    }
  }, 1000);
})();