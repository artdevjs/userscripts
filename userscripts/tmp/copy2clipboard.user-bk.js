// ==UserScript==
// @name         GitHub Copy file to clipboard
// @description  Add copy github file text to clipboard
// @namespace    org.artdev
// @version      0.1
// @author       artdev
// @homepageURL  https://github.com/ldong/github-copy-text
// @supportURL   https://github.com/ldong/github-copy-text/README.md
// @match        *://github.com/*
// @match        *://www.github.com/*
// @match        *://gist.github.com/*/*
// @include      /^https?://git\.corp.*/.*$/
// @run-at       document-end
// @grant        GM_setClipboard
// @grant        GM_xmlhttpRequest
// @icon         https://github.com/favicon.ico
// ==/UserScript==

(function () {
  // copied from http://stackoverflow.com/a/36640126/2305243
  function CopyToClipboard(containerid) {
    let range;
    if (document.selection) {
      range = document.body.createTextRange();
      range.moveToElementText(document.getElementById(containerid));
      range.select().createTextRange();
      document.execCommand('copy');

    } else if (window.getSelection) {
      range = document.createRange();
      range.selectNode(document.querySelector(containerid).nextElementSibling);
      window.getSelection().addRange(range);
      document.execCommand('copy');
      console.log('text copied, copy in the text-area');
    }
  }

  let copyButton = document.createElement('button');
  copyButton.classList = 'btn btn-sm BtnGroup-item copy-file-another';
  copyButton.innerHTML = '<svg aria-hidden="true" class="octicon octicon-clippy" height="16" viewBox="0 0 14 16" width="14"><path fill-rule="evenodd" d="M2 13h4v1H2v-1zm5-6H2v1h5V7zm2 3V8l-3 3 3 3v-2h5v-2H9zM4.5 9H2v1h2.5V9zM2 12h2.5v-1H2v1zm9 1h1v2c-.02.28-.11.52-.3.7-.19.18-.42.28-.7.3H1c-.55 0-1-.45-1-1V4c0-.55.45-1 1-1h3c0-1.11.89-2 2-2 1.11 0 2 .89 2 2h3c.55 0 1 .45 1 1v5h-1V6H1v9h10v-2zM2 5h8c0-.55-.45-1-1-1H8c-.55 0-1-.45-1-1s-.45-1-1-1-1 .45-1 1-.45 1-1 1H3c-.55 0-1 .45-1 1z"></path></svg>';
  copyButton.setAttribute('title', 'Copy file to clipboard');

  let targetButtonGroupClass, copyContentNode;
  if (window.location.href.includes('gist')) {
    targetButtonGroupClass = '.file-actions';
    copyContentNode = '.file > .file-header';
  } else {
    targetButtonGroupClass = '.BtnGroup.d-md-inline-block';
    copyContentNode = '.Box.position-relative > .Box-header';
  }

  copyButton.addEventListener('click', function (e) {
    e.preventDefault();
    CopyToClipboard(copyContentNode);
  });

  let targetButtonGroup = document.querySelector(targetButtonGroupClass);
  console.log(targetButtonGroupClass);
  if (targetButtonGroup) {
    targetButtonGroup.appendChild(copyButton);
  } else {
    console.error('Please file a bug, or make a PR. ');
  }
})();
