// ==UserScript==
// @name         GitHub Copy file to clipboard
// @description  Add copy github file text to clipboard
// @namespace    org.artdev
// @version      0.1
// @author       artdev
// @homepageURL  https://github.com/ldong/github-copy-text
// @supportURL   https://github.com/ldong/github-copy-text/README.md
// @match        *://github.com/*
// @match        *://www.github.com/*
// @match        *://gist.github.com/*/*
// @include      /^https?://git\.corp.*/.*$/
// @grant        GM_setClipboard
// @grant        GM_xmlhttpRequest
// @icon         https://github.com/favicon.ico
// ==/UserScript==

(() => {
  const copyIcon =    '<svg class="octicon octicon-star v-align-text-bottom" viewBox="0 0 14 16" version="1.1" width="14" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M 2 12.5 L 6 12.5 L 6 13.5 L 2 13.5 L 2 12.5 Z M 7 6.5 L 2 6.5 L 2 7.5 L 7 7.5 L 7 6.5 Z M 9 9.5 L 9 7.5 L 6 10.5 L 9 13.5 L 9 11.5 L 14 11.5 L 14 9.5 L 9 9.5 Z M 4.5 8.5 L 2 8.5 L 2 9.5 L 4.5 9.5 L 4.5 8.5 Z M 2 11.5 L 4.5 11.5 L 4.5 10.5 L 2 10.5 L 2 11.5 Z M 11 12.5 L 12 12.5 L 12 14.5 C 11.98 14.78 11.89 15.02 11.7 15.2 C 11.51 15.38 11.28 15.48 11 15.5 L 1 15.5 C 0.45 15.5 0 15.05 0 14.5 L 0 3.5 C 0 2.95 0.45 2.5 1 2.5 L 4 2.5 C 4 1.39 4.89 0.5 6 0.5 C 7.11 0.5 8 1.39 8 2.5 L 11 2.5 C 11.55 2.5 12 2.95 12 3.5 L 12 8.5 L 11 8.5 L 11 5.5 L 1 5.5 L 1 14.5 L 11 14.5 L 11 12.5 Z M 2 4.5 L 10 4.5 C 10 3.95 9.55 3.5 9 3.5 L 8 3.5 C 7.45 3.5 7 3.05 7 2.5 C 7 1.95 6.55 1.5 6 1.5 C 5.45 1.5 5 1.95 5 2.5 C 5 3.05 4.55 3.5 4 3.5 L 3 3.5 C 2.45 3.5 2 3.95 2 4.5 Z"/></svg>';
  const copyButton = document.createElement('button');
  copyButton.setAttribute('title', 'Copy file to clipboard');
  copyButton.classList = 'btn btn-sm BtnGroup-item copy-file-another';
  copyButton.innerHTML = `${copyIcon} Copy File`;

  const copyToClipboard = (el) => {
    const selection = window.getSelection();

    if (document.selection) {
      const range = document.body.createTextRange();

      range.moveToElementText(document.getElementById(el));
      range.select().createTextRange();
      document.execCommand('copy');
      selection.removeAllRanges();
    } else if (window.getSelection) {
      // const referenceNode = document.querySelector(el).nextElementSibling;
      const range = document.createRange();
      const refNode = document.querySelector('.Box.position-relative > .Box-body');

      range.selectNodeContents(refNode);
      selection.removeAllRanges();
      selection.addRange(range);
      document.execCommand('copy');
    }
  };

  const run = () => {
    let targetButtonGroupClass;
    let copyContentNode;
    if (window.location.href.includes('gist')) {
      targetButtonGroupClass = '.file-actions';
      copyContentNode = '.file > .file-header';
    } else {
      targetButtonGroupClass = '.BtnGroup.d-md-inline-block';
      copyContentNode = '.Box.position-relative > .Box-header';
    }

    const targetButtonGroup = document.querySelector(targetButtonGroupClass);
    if (targetButtonGroup) {
      targetButtonGroup.appendChild(copyButton);

      copyButton.addEventListener('click', (e) => {
        e.preventDefault();
        copyToClipboard(copyContentNode);

        setTimeout(() => {
          window.getSelection().removeAllRanges();
        }, 100);
      });
    } else {
      // console.error('Please file a bug, or make a PR. ');
    }
  };

  run();
  document.addEventListener('pjax:end', run, false);
})();

/* #region   */
// const selectElementText = (el) => {
//   const range = document.createRange();
//   range.selectNodeContents(document.querySelector(el).nextElementSibling);
//   const selection = window.getSelection();
//   selection.removeAllRanges();
//   selection.addRange(range);
// };

// const getSelectedText = () => {
//   let t = '';
//   if (window.getSelection) {
//     t = window.getSelection();
//   } else if (document.getSelection) {
//     t = document.getSelection();
//   } else if (document.selection) {
//     t = document.selection.createRange().text;
//   }
//   return t;
// };

// const copyToSelection = (text) => {
//   if (window.clipboardData && window.clipboardData.setData) {
//     // IE specific code path to prevent textarea being shown while dialog is visible.
//     return clipboardData.setData('Text', text);
//   } else if (document.queryCommandSupported && document.queryCommandSupported('copy')) {
//     const textarea = document.createElement('textarea');
//     textarea.textContent = text;
//     textarea.style.position = 'fixed'; // Prevent scrolling to bottom of page in MS Edge.
//     document.body.appendChild(textarea);
//     textarea.select();
//     try {
// eslint-disable-next-line
//       return document.execCommand('copy'); // Security exception may be thrown by some browsers.
//     } catch (ex) {
//       console.warn('Copy to clipboard failed.', ex);
//       return false;
//     } finally {
//       document.body.removeChild(textarea);
//     }
//   }
// };
/* #endregion */
