// ==UserScript==
// @name            GitHub Copy Buttons
// @description     Add 'Copy File' and 'Copy Raw Url' buttins
// @version         0.1.0
// @author          artdev
// @namespace       https://gitlab.com/artdevjs
// @homepageURL     https://gitlab.com/artdevjs/tampermonkey-userscripts
// @supportURL      https://gitlab.com/artdevjs/tampermonkey-userscripts/issues
// @license         MIT
// @match           https://github.com/*
// @match           https://gist.github.com/*/*
// @run-at          document-idle
// @grant           GM_setClipboard
// @grant           GM_notification
// @grant           GM_xmlhttpRequest
// @icon            https://github.com/favicon.ico
// @require         https://greasyfork.org/scripts/28721-mutations/code/mutations.js?version=666427
// ==/UserScript==

(() => {
  "use strict";

  const copyIcon = `
    <svg class="octicon octicon-star v-align-text-bottom" viewBox="0 0 14 16" version="1.1" width="14" height="16" aria-hidden="true">
      <path fill-rule="evenodd" d="M 2 12.5 L 6 12.5 L 6 13.5 L 2 13.5 L 2 12.5 Z M 7 6.5 L 2 6.5 L 2 7.5 L 7 7.5 L 7 6.5 Z M 9 9.5 L 9 7.5 L 6 10.5 L 9 13.5 L 9 11.5 L 14 11.5 L 14 9.5 L 9 9.5 Z M 4.5 8.5 L 2 8.5 L 2 9.5 L 4.5 9.5 L 4.5 8.5 Z M 2 11.5 L 4.5 11.5 L 4.5 10.5 L 2 10.5 L 2 11.5 Z M 11 12.5 L 12 12.5 L 12 14.5 C 11.98 14.78 11.89 15.02 11.7 15.2 C 11.51 15.38 11.28 15.48 11 15.5 L 1 15.5 C 0.45 15.5 0 15.05 0 14.5 L 0 3.5 C 0 2.95 0.45 2.5 1 2.5 L 4 2.5 C 4 1.39 4.89 0.5 6 0.5 C 7.11 0.5 8 1.39 8 2.5 L 11 2.5 C 11.55 2.5 12 2.95 12 3.5 L 12 8.5 L 11 8.5 L 11 5.5 L 1 5.5 L 1 14.5 L 11 14.5 L 11 12.5 Z M 2 4.5 L 10 4.5 C 10 3.95 9.55 3.5 9 3.5 L 8 3.5 C 7.45 3.5 7 3.05 7 2.5 C 7 1.95 6.55 1.5 6 1.5 C 5.45 1.5 5 1.95 5 2.5 C 5 3.05 4.55 3.5 4 3.5 L 3 3.5 C 2.45 3.5 2 3.95 2 4.5 Z"/>
    </svg>`;

  const	copyButton = document.createElement("a");
  copyButton.className = "btn btn-sm BtnGroup-item";

  let btnsSelector;
  if (location.host === 'github.com') {
    btnsSelector = '.Box-header .BtnGroup';
  } else if (location.host === 'gist.github.com') {
    btnsSelector = '.file-actions';
  }

  function copyCode(url) {
    GM_xmlhttpRequest({
      method: 'GET',
      url: url,
      onload: e => {
        if (e.status === 200) {
          GM_setClipboard(e.responseText);
          GM_notification('Copied!', 'Copy File Content');
        } else {
          GM_notification(`error code: ${e.status}`);
        }
      }
    });
  }

  function addButtons(a) {
    const wrap = a.parentElement;

    if (!wrap.classList.contains("gh-copy-wrap")) {
      const copyFile = copyButton.cloneNode(true);
      const copyRawUrl  = copyButton.cloneNode(true);

      copyFile.innerHTML = `${copyIcon} File`;
      copyFile.setAttribute('title', 'Copy file to clipboard');
      copyRawUrl.innerHTML = `${copyIcon} Raw Url`;
      copyRawUrl.setAttribute('title', 'Copy Raw Url to clipboard');
      copyFile.addEventListener('click', e => copyCode(a.href));
      copyRawUrl.addEventListener('click', e => GM_setClipboard(a.href));

      wrap.classList.add("gh-copy-wrap");
      wrap.insertBefore(copyRawUrl, wrap.childNodes[0]);
      wrap.insertBefore(copyFile, wrap.childNodes[0]);
    }
  }

	function init() {
    const btns = document.querySelectorAll(btnsSelector);
		if (btns.length > 0) {
			[...document.querySelectorAll(btnsSelector)].forEach(btn => {
        [...btn.querySelectorAll('a')].forEach(a => {
          if ( a.innerText === 'Raw' ) {
            addButtons(a);
          }
				});
			});
		}
	}

	document.addEventListener("ghmo:container", init);
  init();
})();
