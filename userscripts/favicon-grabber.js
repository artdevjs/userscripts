// ==UserScript==
// @name            Favicon Grabber
// @description     Grabbing favicons from any domain
// @version         0.1.0
// @author          artdev
// @namespace       https://gitlab.com/artdevjs
// @homepageURL     https://gitlab.com/artdevjs/tampermonkey-userscripts
// @supportURL      https://gitlab.com/artdevjs/tampermonkey-userscripts/issues
// @license         MIT
// @include         http*://*
// @run-at          document-end
// @icon            https://tampermonkey.net/favicon.ico
// @grant           GM_addStyle
// @grant           GM_setClipboard
// @grant           GM_notification
// @grant           GM_xmlhttpRequest
// ==/UserScript==

(function() {
  "use strict";

  function grabFavicons(url) {
    GM_xmlhttpRequest({
      method: "GET",
      url: `https://favicongrabber.com/api/grab/${url}`,
      responseType: "json",
      onload: e => {
        if (e.status === 200) {
          let res = e.response;
          console.log(res.icons);
        } else {
          GM_notification(`error code: ${e.status}`);
        }
      }
    });
  }

  function inlineBase64Images(img) {
    GM_xmlhttpRequest({
      method: 'GET',
      url: img.src,
      responseType: 'blob',
      onload: function(xhr) {

        if (xhr.status !== 200) return res(null)

        let headers = {}
        for (let header of xhr.responseHeaders.trim().split(/\n/)) {
          let parts = header.split(':')
          if (parts.length === 1) continue
          headers[parts[0].trim()] = parts[1].trim()
        }

        let type = headers['Content-Type']

        if (!/^image\//.test(type)) {
          if (/\.jpe?g$/i.test(img.src)) type = 'image/jpeg'
          if (/\.png$/i.test(img.src)) type = 'image/png'
          if (/\.gif$/i.test(img.src)) type = 'image/png'
        }

        if (!/^image\//.test(type)) {
          console.error(`Can't process "${img.src}" because its type is "${type}"`)
          return res(null)
        }

        let imgEl = new Image()
        imgEl.onload = function () {

          let canvas = document.createElement('canvas');
          canvas.width = imgEl.naturalWidth;
          canvas.height = imgEl.naturalHeight;

          try {
            canvas.getContext('2d').drawImage(imgEl, 0, 0)
            img.src = canvas.toDataURL(type, 0.7)
            res(img)
          } catch (e) {
            console.error(`Can't draw "${img.src}" on a canvas`, e)
            res(null)
          }

        }
        imgEl.onerror = () => res(null)
        imgEl.src = window.URL.createObjectURL(xhr.response)

      }
    })
  }

  document.addEventListener("DOMContentLoaded", e => {
    GM_addStyle(`
      .favicon-grabber {
        width: 3rem;
        height: 3rem;
        position: fixed;
        border-radius: 50%;
        right: 32px;
        bottom: 64px;
        left: unset;
        cursor: pointer;
        transition: all 0.5s ease;
      }
    `);

    let btn = document.createElement("button");
    btn.classList = "favicon-grabber";
    btn.innerText = "Get Favs";
    btn.setAttribute("title", "Grab Favicons for current website");

    btn.addEventListener("click", e => {
      e.preventDefault();
      // grabFavicons(window.location.hostname);
      inlineBase64Images("https://github.githubassets.com/favicon.ico");
    });

    document.body.appendChild(btn);
  });



  // let startTime = +new Date
  // inlineBase64Images(document.querySelectorAll('img')).then(imgs => {
  //   console.log(imgs.length + ' images converted in ' + ((+new Date) - startTime) / 1000 + 's')
  // })
})();
