// ==UserScript==
// @name            AdguardHome: Encrese filters
// @description     Show filters per 100 rows
// @version         0.1
// @author          artdev
// @namespace       https://gitlab.com/artdevjs
// @homepageURL     https://gitlab.com/artdevjs/tampermonkey-userscripts
// @supportURL      https://gitlab.com/artdevjs/tampermonkey-userscripts/issues
// @license         MIT
// @include         https?://(localhost|127.0.0.1|adwrt.cf)/*
// @run-at          document-end
// @grant           none
// @icon            https://adguard.com/favicon.ico
// ==/UserScript==

(function() {
  "use strict";

  // const run = () => {
  //   if (location.hash != "#filters") return;

  //   document.querySelector("select[aria-label='rows per page']").value = 100;
  // };

  // // run();

  // document.addEventListener('DOMContentLoaded', (e) => {
  //   console.log('DOMContentLoaded');
  //   run();
  // });

  let waitForSomeElement = () => {
    const el = document.querySelector("select[aria-label='rows per page']");

    if (!el) {
      setTimeout(waitForSomeElement, 50); // give everything some time to render
    }

    el.value = 100;
  };
  waitForSomeElement();
})();
