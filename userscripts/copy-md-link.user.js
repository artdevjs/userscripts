// ==UserScript==
// @name            Copy selected text and URL as Markdown link
// @description     Copy the current URL and selected text in a Markdown format with Alt-L.
// @version         0.1.0
// @author          artdev
// @namespace       https://gitlab.com/artdevjs
// @homepageURL     https://gitlab.com/artdevjs/tampermonkey-userscripts
// @supportURL      https://gitlab.com/artdevjs/tampermonkey-userscripts/issues
// @license         MIT
// @include         *://*/*
// @run-at          document-end
// @grant           GM_setClipboard
// @grant           GM_notification
// @icon            data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAADF0lEQVRoge2YW08TURDH+U6z0HJ9QlAuBiFtQYpgMHhpAbkIVORBk5KgCY1QE7YES4BCpG2QS7gIAm2AgraKsdRopJiYyMcYH5BRdLe7xbKnJPswLzOz/3N+u3vOzDkph9EQnmdLYT0BFYD1BFQA1hNQAVhPQAVgPYEzA/j2KYBO3iZoB5EtyltbdAvmLEyPiQ66u72EbpcDvROD+GM/iIfREAbWZ9DtcuD81GhiAMLB1wgAghYOrlCe9aFFMOdu/U3RQa8ZyxEAsL21nnwbK1PIcRxyHIf+ZW9iAfIu5GJtjZHs8wcf5Tl5G/mvV1+VBPCMOxAAUJOWhu8DSydiptu1CABYXVWRWIAHlmZZYtHIZkyA7192sLjwEgIAdnY0/RMPrM9gamoqAgB6JwaTD2B48CnFd3yzghrHX1FXVpJ8AAZdKQIAGnSlohojQ32ksbboTh4A/7KXYgN9PaIa+3ubmJGRjgCAbS3m5AHosjRT7OPblZg6pltHizk9XXtiy2YKUFxUgACA+Xm5kjr9vVbSmX8pXk8UA4i8WyN/o7lOUmd5boLye6xd7AHcLgf5+3utkjoHkS3kOA4BAKsqDewB7LZu8o8PP5OllZOTTUWUOcCf7caMZ1iWVlHBxaOKrdFQv8QMoK3FTP7VhUlZWuX6Mnrma3iDLUB7a/1vgMX4Afb3NtkCdD+6T/5pt1OWVmFBPgIAarVa9mvgVIs4O0tW3VAEYHKMJ7/d1i2pk3Tb6F5olfwNpvgK2eNkKGSH0RBePm0rIXHEVAygs6OJYmGJZs5858avBazBaER8B1IUYH3JQzHe/iTm/5+VmYkAgK1NJskxFT3Q6HVXEACwXF8mqjH23B5X0VMUwMnbKP7GPyeoUVtjRADA0pJiWWMqfqg/7nG6BDR3/LN0qHe7HIkDSOS1yovRAWrSdrdPXqs0mOoQANBYoY/ZwMUN8Lf978VWVaUBAQAt9xrJt+07evscx8lu+GICnOXVYnBjHp28DUeG+uhN+1550cnb0DMu79eRBDgvpgKwNhWAtakArE0FYG3nHuAnLzKpfJnlJmcAAAAASUVORK5CYII=
// ==/UserScript==


(function() {
  'use strict'

  function main() {
    function copyTextToClipboard(text) {
      var textArea = document.createElement("textarea");
      textArea.style.position = 'fixed';
      textArea.style.top = 0;
      textArea.style.left = 0;
      textArea.style.width = '2em';
      textArea.style.height = '2em';
      textArea.style.padding = 0;
      textArea.style.border = 'none';
      textArea.style.outline = 'none';
      textArea.style.boxShadow = 'none';
      textArea.style.background = 'transparent';
      textArea.value = text;
      document.body.appendChild(textArea);
      textArea.select();
      try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
        GM_notification('Copied!', 'Copy Url for Markdown');
      }
      catch (err) {
        console.log('Oops, unable to copy');
      }
      document.body.removeChild(textArea);
    };

    var url = encodeURIComponent(location.href);
    url = url.replace(/%3A/g, ':').replace(/%2F/g, '/');
    var title = document.title;
    title = title.replace(/\[/g, '{'); title = title.replace(/\]/g, '}');
    // see https://unicodelookup.com for unicode values
    title = title.replace(/[\u2012]/g, "-"); // figure dash
    title = title.replace(/[\u2013]/g, "-"); // en dash
    title = title.replace(/[\u2014]/g, "--"); // em dash
    title = title.replace(/[\u00B7]/g, "-"); // dot
    title = title.replace(/[\u2019]/g, "'"); // apostrophe

    var sel_text = window.getSelection();
    sel_text = sel_text.toString() ? " - " + sel_text : "";

    copyTextToClipboard('* ['+title+']('+url+')' + sel_text);
  }

  // listen for Alt-C key-combination, and then execute
  document.onkeyup=function(e){
    var e = e || window.event; // for IE to cover IEs window object
    if(e.altKey && e.which == 76) {
      main();
      return false;
    }
  }
})();
