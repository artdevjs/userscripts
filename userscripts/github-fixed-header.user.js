// ==UserScript==
// @name            GitHub Fixed Header
// @description     Add a pinned header on GitHub
// @version         1.2.14
// @author          StylishThemes <https://github.com/StylishThemes>
// @namespace       https://github.com/StylishThemes
// @homepageURL     https://github.com/StylishThemes/GitHub-FixedHeader
// @supportURL      https://github.com/StylishThemes/GitHub-FixedHeader/issues
// @license         CC-BY-SA-4.0
// @include         https://*.github.com/*
// @run-at          document-end
// @grant           GM_addStyle
// @icon            https://github.com/favicon.ico
// ==/UserScript==

(function() {
  'use strict';

  document.addEventListener('DOMContentLoaded', function(e) {
    if (false || (new RegExp("^https?://((gist|guides|help|raw|status|developer)\.)?github\.com/((?!generated_pages/preview).)*$")).test(document.location.href)) {
      GM_addStyle(":root{--height: 57px}.is-stuck,body:not(.project-full-screen){padding-top:var(--height)!important}div.pr-toolbar.is-stuck+.pr-toolbar-shadow{background:#fff;top:var(--height)!important;height:40px!important;border-bottom:1px solid #ddd}.table-list-header.table-list-header-next.js-sticky.is-stuck{height:calc(var(--height) + 50px)}.diff-view .file-info a{text-overflow:ellipsis;max-width:490px;white-space:nowrap;overflow:hidden;display:inline-block}.file-header:before{clear:both}.diff-view .file-info span.diffstat{top:-11px!important}div.is-stuck .diffbar{margin-top:-12px!important}.diff-view .file-header{height:40px!important}.diff-view .file-info{position:absolute;top:4px!important}.diff-view .file-actions{position:absolute;top:5px!important;right:5px!important}div.diffbar{padding-top:20px!important;padding-bottom:20px!important}@supports (position:sticky){#files .file-header{position:sticky!important;position:-webkit-sticky!important;top:95px!important;height:40px!important;z-index:6!important}}#toc+#files .file-header,.user-profile-sticky-bar:after,div.blog-aside,div.discussion-sidebar{top:var(--height)!important}.pjax-loader-bar,.pjax-loader-bar .progress{z-index:1001!important}.accessibility-aid,a.anchor{padding-top:55px!important;margin-top:-55px!important;pointer-events:none;min-width:5px!important;display:inline-block!important}a.anchor span,a.anchor svg{pointer-events:all!important;min-width:20px!important}.site-header{padding-top:10px!important;padding-bottom:10px!important}.site-header-nav-main .nav-item:after{bottom:-10px!important}#js-flash-container+.main-content{margin-top:0!important}#com #header,.Header,.site-header,body.logged-in .header{position:fixed!important;top:0!important;left:0!important;right:0!important;width:100%!important;z-index:1000!important}.notifications-dropdown{z-index:1001!important}.octotree-show .Header .container,.octotree-show .header .container{margin-left:auto!important}");
    }
    if (false || (document.location.href.indexOf("https://gist.github.com") == 0)) {
      GM_addStyle("@supports (position:sticky){#files .file-header{top:var(--height)!important}}");
    }
  });
})();
