// ==UserScript==
// @name            reCaptcha Solver
// @description     This solve google recaptcha automatic after sec
// @version         0.1.0
// @author          artdev
// @namespace       https://gitlab.com/artdevjs
// @homepageURL     https://gitlab.com/artdevjs/tampermonkey-userscripts
// @supportURL      https://gitlab.com/artdevjs/tampermonkey-userscripts/issues
// @license         MIT
// @include         *
// @run-at          document-end
// @grant           none
// @icon            https://thirtybees.com/wp-content/uploads/2017/05/reCAPTCHA_cleaned.png
// ==/UserScript==

(function() {
  'use strict'

  function isScrolledIntoView(el) {
    let docViewTop     = window.scrollY;
    let docViewBottom  = docViewTop + window.innerHeight;
    let elemTop        = el.getBoundingClientRect().top;
    let elemBottom     = elemTop + el.clientHeight;

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
  }

  function isVisible(el) {
    return !!( el.offsetWidth || el.offsetHeight || el.getClientRects().length );
  }

  /**
   * Simulate a click event.
   * @public
   * @param {Element} elem  the element to simulate a click on
   */
  let simulateClick = function (elem) {
    // Create our event (with options)
    let evt = new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
      view: window
    });
    // If cancelled, don't dispatch our event
    // eslint-disable-next-line no-unused-vars
    let canceled = !elem.dispatchEvent(evt);
  };

  let sid = setInterval(function() {
    let el = document.querySelector('#recaptcha-anchor div.recaptcha-checkbox-checkmark');

    if ( window.location.href.match(/https:\/\/www.google.com\/recaptcha\/api\d\/anchor/) &&
        el !== null && isVisible(el) && isScrolledIntoView(el) ) {
      let execute = true;

      if (sessionStorage.getItem('accesstime')) {
        if ( new Date().getTime() - sessionStorage.getItem('accesstime') < 7000 ) {
          execute = false;
        }
      }

      if (execute) {
        simulateClick(el);
        sessionStorage.setItem('accesstime', new Date().getTime());
      }
      clearInterval(sid);
    }
  }, 1500);
})();
