// ==UserScript==
// @name            Script name
// @description     Description
// @version         0.1.0
// @author          artdev
// @namespace       https://gitlab.com/artdevjs
// @homepageURL     https://gitlab.com/artdevjs/tampermonkey-userscripts
// @supportURL      https://gitlab.com/artdevjs/tampermonkey-userscripts/issues
// @license         MIT
// @include         *
// @run-at          document-end
// @grant           GM_addStyle
// @icon            https://tampermonkey.net/favicon.ico
// ==/UserScript==

(() => {
  'use strict';

  document.addEventListener('DOMContentLoaded', e => {
    // silently fails in Firefox if placed outside when `document-start`
    GM_addStyle(`
      
    `);
  });
})();
