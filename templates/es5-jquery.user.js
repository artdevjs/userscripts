// ==UserScript==
// @name            
// @description     Description
// @version         0.1.0
// @author          artdev
// @namespace       https://gitlab.com/artdevjs
// @homepageURL     https://gitlab.com/artdevjs/tampermonkey-userscripts
// @supportURL      https://gitlab.com/artdevjs/tampermonkey-userscripts/issues
// @license         MIT
// @match           http*://
// @run-at          document-end
// @grant           none
// @require         https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js
// @icon            https://tampermonkey.net/favicon.ico
// ==/UserScript==

/* jshint asi: true, esnext: true, -W097 */

(function($) {
  'use strict'

  // Code...

}).bind(this)(jQuery)
  
jQuery.noConflict()
