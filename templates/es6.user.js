// ==UserScript==
// @name            reCaptcha Clicker Lite
// @description     This solve google recaptcha automatic after sec
// @version         0.1.0
// @author          artdev
// @namespace       https://gitlab.com/artdevjs
// @homepageURL     https://gitlab.com/artdevjs/tampermonkey-userscripts
// @supportURL      https://gitlab.com/artdevjs/tampermonkey-userscripts/issues
// @license         MIT
// @include         *://
// @run-at          document-end
// @grant           none
// @require         https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.18.2/babel.js
// @require         https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.16.0/polyfill.js
// @icon            https://thirtybees.com/wp-content/uploads/2017/05/reCAPTCHA_cleaned.png
// ==/UserScript==

let inline_src = (<><![CDATA[

]]></>).toString();
let c = Babel.transform(inline_src, { presets: [ "es2015", "es2016" ] });
eval(c.code);
