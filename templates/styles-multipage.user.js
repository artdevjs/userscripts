// ==UserScript==
// @name            ScriptName
// @description     Description
// @version         0.1.0
// @author          artdev
// @namespace       https://gitlab.com/artdevjs
// @homepageURL     https://gitlab.com/artdevjs/tampermonkey-userscripts
// @supportURL      https://gitlab.com/artdevjs/tampermonkey-userscripts/issues
// @license         MIT
// @match           http*://
// @run-at          document-end
// @grant           GM_addStyle
// @icon            https://tampermonkey.net/favicon.ico
// ==/UserScript==

(() => {
  'use strict';

  document.addEventListener('DOMContentLoaded', e => {
    if (false || (new RegExp("")).test(document.location.href)) {
      GM_addStyle(`

      `);
    }
    if (false || (document.location.href.indexOf("") == 0)) {
      GM_addStyle(`

      `);
    }
  });
})();
