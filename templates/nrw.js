// ==UserScript==
// @name            AdguardHome: Encrese filters
// @description     Show filters per 100 rows
// @version         0.1
// @author          artdev
// @namespace       https://gitlab.com/artdevjs
// @homepageURL     https://gitlab.com/artdevjs/tampermonkey-userscripts
// @include         https?://(localhost|127.0.0.1|adwrt.cf)/*
// @require         https://greasyfork.org/scripts/38888-greasemonkey-color-log/code/Greasemonkey%20%7C%20Color%20Log.js
// @require         https://greasyfork.org/scripts/38889-greasemonkey-waitforkeyelements-2018/code/Greasemonkey%20%7C%20waitForKeyElements%202018.js
// @require         https://greasyfork.org/scripts/374849-library-onelementready-es6/code/Library%20%7C%20onElementReady%20ES6.js
// @run-at          document-end
// @icon            https://adguard.com/favicon.ico
// ==/UserScript==

/**
 * @default
 * @constant
 */
DEBUG = false;


/**
* Init
*/
let init = () => {
  console.debug('init')

  // Ready
  onElementReady('select[aria-label="rows per page"]', false, (element) => {
    console.debug('elementReady: ', element);

    console.debug('element.value: ', element.value);

    element.value = 100;
  })
}

/**
* @listens window:Event#load
*/
window.addEventListener('load', () => {
  console.debug('window#load')

  init()
})
