# Bookmarklets

[Bookmarklets](https://en.wikipedia.org/wiki/Bookmarklet) are like bookmarks, except the link is JavaScript code.

To save a bookmarklet, save the code as a bookmark or drag the code into bookmark bar.

## md: copy url

```js
javascript:/*md: copy url*/ (function(){%20window.prompt(%22Press%20Ctrl+C%20then%20Enter%20to%20close%20the%20dialog%22,'['%20+%20document.title%20+%20']('%20+%20document.location.href%20+%20')');%20})();
```

## md: link

```js
javascript:/*md: link*/ (function(){var t=window.getSelection().toString(),o=t||document.title;prompt(%22%22,%22[%22+o+%22](%22+location.href+%22)%22)})();
```

## md: hover

```js
javascript:/*md: hover*/ (function()%7Bvar%20p%3Ddocument.createElement(%22p%22)%3Bp.innerHTML%3D%22%3Cstrong%3ELoading%E2%80%A6%3C/strong%3E%22%3Bp.id%3D%22loadingp%22%3Bp.style.padding%3D%2220px%22%3Bp.style.background%3D%22%23fff%22%3Bp.style.left%3D%2220px%22%3Bp.style.top%3D0%3Bp.style.position%3D%22fixed%22%3Bp.style.zIndex%3D%229999999%22%3Bp.style.opacity%3D%22.85%22%3Bdocument.body.appendChild(p)%3Bdocument.body.appendChild(document.createElement(%22script%22)).src%3D%22//d2z4mgf8zqkbu2.cloudfront.net/bookmarklets/bullseye.js%3Fx%3D%22%2B(Math.random())%3B%7D)()%3B
```

## md: selected

```js
javascript:/*md: selected*/ var%20markerShowFrame%3D0%3B(function()%7Bvar%20p%3Ddocument.createElement(%22p%22)%3Bp.innerHTML%3D%22%3Cstrong%3ELoading%E2%80%A6%3C/strong%3E%22%3Bp.id%3D%22loadingp%22%3Bp.style.padding%3D%2220px%22%3Bp.style.background%3D%22%23fff%22%3Bp.style.left%3D%2220px%22%3Bp.style.top%3D0%3Bp.style.position%3D%22fixed%22%3Bp.style.zIndex%3D%229999999%22%3Bp.style.opacity%3D%22.85%22%3Bdocument.body.appendChild(p)%3Bdocument.body.appendChild(document.createElement(%22script%22)).src%3D%22//d2z4mgf8zqkbu2.cloudfront.net/bookmarklets/marker.js%3Fx%3D%22%2B(Math.random())%3B%7D)()%3B
```

## md: quite

```js
javascript:/*md: quote*/ (function(){javascript:(function(){ if (window.mo) { } window.mo = { }; var version = 1; rand = Math.random(); var uniqueId = %27gwtw-bookmarklet%27; var script = document.getElementById(uniqueId); if (script) { script.parentNode.removeChild(script); } script = document.createElement(%27script%27); script.setAttribute(%27type%27, %27text/javascript%27); script.setAttribute(%27charset%27, %27UTF-8%27); script.setAttribute(%27src%27, %27\/\/cdn.rawgit.com/marcus-at-localhorst/fa95d030919558062469e56cff468388/raw/f4caa7385a5cdb0627f8cbc9bcc6dcee93ec63a8/bookmarklet.js?r=%27 + rand); script.id = uniqueId; document.documentElement.appendChild(script); script.onload = script.onreadystatechange = function() { var rs = script.readyState; if (!rs || rs === %27loaded%27 || rs === %27complete%27) { script.onload = script.onreadystatechange = null; if (version !== window.mo.version) { alert(%27This bookmarklet is out of date!%27); } else { window.mo.init(); } } }; }());})();
```

## Inoreader: Subscribe

```js
javascript:/*Subscribe in Inoreader*/ function p(a,w,h){var b=window.screenLeft!=undefined?window.screenLeft:screen.left;var c=window.screenTop!=undefined?window.screenTop:screen.top;width=window.innerWidth?window.innerWidth:document.documentElement.clientWidth?document.documentElement.clientWidth:screen.width;height=window.innerHeight?window.innerHeight:document.documentElement.clientHeight?document.documentElement.clientHeight:screen.height;var d=((width/2)-(w/2))+b;var e=((height/2)-(h/2))+c;var f=window.open(a,new Date().getTime(),'width='+w+', height='+h+', top='+e+', left='+d+'location=yes,resizable=yes,status=no,scrollbars=no,personalbar=no,toolbar=no,menubar=no');if(window.focus){f.focus()}}p('https://www.inoreader.com/bookmarklet/subscribe/'+encodeURIComponent(location.href),640,400);
```

## Inoreader: Save to

```js
javascript:/*Save to Inoreader*/ function p(a,w,h){var b=window.screenLeft!=undefined?window.screenLeft:screen.left;var c=window.screenTop!=undefined?window.screenTop:screen.top;width=window.innerWidth?window.innerWidth:document.documentElement.clientWidth?document.documentElement.clientWidth:screen.width;height=window.innerHeight?window.innerHeight:document.documentElement.clientHeight?document.documentElement.clientHeight:screen.height;var d=((width/2)-(w/2))+b;var e=((height/2)-(h/2))+c;var f=window.open(a,new Date().getTime(),'width='+w+', height='+h+', top='+e+', left='+d+'location=yes,resizable=yes,status=no,scrollbars=no,personalbar=no,toolbar=no,menubar=no');if(window.focus){f.focus()}}p('https://www.inoreader.com/bookmarklet/save_web_page/'+encodeURIComponent(location.href),640,400);
```

## Stylus Auditor

```js
javascript:/*stylys auditor*/ ujs_stylusAuditor()
```

## Hypothesis

```js
javascript:/*Hypothesis*/(function(){window.hypothesisConfig=function(){return{showHighlights:true,appType:'bookmarklet'};};var d=document,s=d.createElement('script');s.setAttribute('src','https://hypothes.is/embed.js');d.body.appendChild(s)})();
```

## about:config changed

```js
javascript:/*about:config*/ (function(t){var e={eval:'%22!function(e){window.uF?e.removeRule(0):e.insertRule(\\%22tr:not(.has-user-value){display:none}\\%22),window.uF=!window.uF}(document.styleSheets[2]);%22'},o=!0;if(%22object%22==typeof this.artoo&&(artoo.settings.reload||(artoo.log.verbose(%22artoo already exists within this page. No need to inject him again.%22),artoo.loadSettings(e),artoo.exec(),o=!1)),o){var i=document.getElementsByTagName(%22body%22)[0];i||(i=document.createElement(%22body%22),document.firstChild.appendChild(i));var a=document.createElement(%22script%22);console.log(%22artoo.js is loading...%22),a.src=%22//medialab.github.io/artoo/public/dist/artoo-latest.min.js%22,a.type=%22text/javascript%22,a.id=%22artoo_injected_script%22,a.setAttribute(%22settings%22,JSON.stringify(e)),i.appendChild(a)}}).call(this);
```

## jQuerify

```js
javascript:/*jQuerify*/ (function()%7B%22use strict%22;function t()%7Bconsole.log(%22jQuery loaded!%22)%7Dvar e;e=document.createElement(%22script%22);e.addEventListener(%22load%22,t);e.src=%22//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js%22;document.head.appendChild(e)%7D())
```

## Printliminator

```js
javascript:/*Printliminator*/ (function(){function%20loadScript(a,b){var%20c=document.createElement('script'),d=document.getElementsByTagName('head')[0],e=!1;c.type='text/javascript',c.src=a,c.onload=c.onreadystatechange=function(){e||this.readyState&&'loaded'!=this.readyState&&'complete'!=this.readyState||(e=!0,b())},d.appendChild(c)}loadScript('//css-tricks.github.io/The-Printliminator/printliminator.min.js',function(){thePrintliminator.init()});})();
